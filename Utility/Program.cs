﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] parameter = args[0].Split('|');
            string Destpath = ConfigurationManager.AppSettings["destinationPath"] + parameter[1];

            switch (parameter[0])
            {
                case "Copy":
                    DirectoryCopy(ConfigurationManager.AppSettings["sourcePath"] + parameter[1], Destpath);
                    break;
                case "Delete":
                    if (Directory.Exists(Destpath))
                        Directory.Delete(Destpath, true);
                    break;
                case "CopyThumbnail":
                    Destpath = ConfigurationManager.AppSettings["ThumbnailDest"] + parameter[1];
                    File.Copy(ConfigurationManager.AppSettings["Thumbnailsource"] + parameter[1], Destpath, true);
                    break;
                case "DeleteThumbnail":
                    Destpath = ConfigurationManager.AppSettings["ThumbnailDest"] + parameter[1];
                    if (File.Exists(Destpath))
                        File.Delete(Destpath);
                    break;
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName)
        {
            try
            {
                // Get the subdirectories for the specified directory.
                DirectoryInfo dir = new DirectoryInfo(sourceDirName);

                if (!dir.Exists)
                {
                    throw new DirectoryNotFoundException(
                        "Source directory does not exist or could not be found: "
                        + sourceDirName);
                }

                DirectoryInfo[] dirs = dir.GetDirectories();
                // If the destination directory doesn't exist, create it.
                if (!Directory.Exists(destDirName))
                {
                    Directory.CreateDirectory(destDirName);
                }

                // Get the files in the directory and copy them to the new location.
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string temppath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(temppath, false);
                }

                // If copying subdirectories, copy them and their contents to new location.
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath);
                }
            }
            catch (Exception ex)
            {
                string FileName = DateTime.Now.ToString("yyyyMMdd")+".txt";
                string Path = ConfigurationManager.AppSettings["LogFolder"] + FileName + ".txt";

                File.AppendAllText(Path, DateTime.Now  + ": Stacktrace =>" + Environment.NewLine + ex.StackTrace + Environment.NewLine+"Message =>"+ex.Message + Environment.NewLine);
                File.AppendAllText(Path, "------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);

            }
        }
    }
}
