﻿using Swunmath.Model;
using System;
using System.Text;
using System.Web.Mvc;

namespace Swunmath.Filters
{
    public class LoggingFilterAttribute : ActionFilterAttribute,IExceptionFilter
    {
        StringBuilder message = new StringBuilder();
        #region Logging
        /// <summary>
        /// Access to the log4Net logging object
        /// </summary>
        //protected static readonly log4net.ILog log =
        //  log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private const string StopwatchKey = "DebugLoggingStopWatch";

        #endregion

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {            
            message.Append(string.Format("Executing controller {0}, action {1}",
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
            Logger.WriteLog(message.ToString(),"Info");
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {            
            message.Append(string.Format("Finished executing controller {0},action {1}",
                        filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                        filterContext.ActionDescriptor.ActionName));
            Logger.WriteLog(message.ToString(), "Info");
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is NullReferenceException)
            {
                //filterContext.Result = new RedirectResult("customErrorPage.html");
                message.Append(string.Format("Finished executing controller {0} :",
                       filterContext.Exception));
                Logger.WriteLog(message.ToString(), "Error");
                //filterContext.Result = new RedirectResult("customErrorPage.html");
                filterContext.ExceptionHandled = true;
            }
        }
    }
}