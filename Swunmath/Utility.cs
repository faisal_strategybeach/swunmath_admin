﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Swunmath
{
    public class Utility
    {
        public string Encrypt(string plainText)
        {
            //geeks@strategybeach.com 12052014 : SQL vulnerabilities resolved
            MemoryStream memoryStream = new MemoryStream();

            try
            {
                string passPhrase = "Pas5pr@se";        // can be any string
                string saltValue = "s@1tValue";        // can be any string
                string hashAlgorithm = "SHA1";             // can be "MD5"
                int passwordIterations = 2;                // can be any number
                string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
                int keySize = 256;                // can be 192 or 128
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                PasswordDeriveBytes password = new PasswordDeriveBytes
                (
                    passPhrase,
                    saltValueBytes,
                    hashAlgorithm,
                    passwordIterations
                );

                byte[] keyBytes = password.GetBytes(keySize / 8);

                RijndaelManaged symmetricKey = new RijndaelManaged();

                symmetricKey.Mode = CipherMode.CBC;

                ICryptoTransform encryptor = symmetricKey.CreateEncryptor
                (
                    keyBytes,
                    initVectorBytes
                );

                CryptoStream cryptoStream = new CryptoStream
                (
                    memoryStream,
                    encryptor,
                    CryptoStreamMode.Write
                );

                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                cryptoStream.FlushFinalBlock();

                cryptoStream.Close();

                byte[] cipherTextBytes = memoryStream.ToArray();

                //string cipherText = Convert.ToBase64String(cipherTextBytes);
                string cipherText = HttpServerUtility.UrlTokenEncode(cipherTextBytes);

                return cipherText;
            }
            catch (Exception)
            {
                return "";
            }
            finally
            {
                memoryStream.Close();

            }
        }

        public string Decrypt(string cipherText)
        {
            MemoryStream memoryStream = new MemoryStream();
            try
            {
                //geeks@strategybeach.com 12052014 : SQL vulnerabilities resolved

                string passPhrase = "Pas5pr@se";        // can be any string
                string saltValue = "s@1tValue";        // can be any string
                string hashAlgorithm = "SHA1";             // can be "MD5"
                int passwordIterations = 2;                // can be any number
                string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
                int keySize = 256;                // can be 192 or 128
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                byte[] cipherTextBytes = HttpServerUtility.UrlTokenDecode(cipherText);

                PasswordDeriveBytes password = new PasswordDeriveBytes
                (
                    passPhrase,
                    saltValueBytes,
                    hashAlgorithm,
                    passwordIterations
                );

                byte[] keyBytes = password.GetBytes(keySize / 8);

                RijndaelManaged symmetricKey = new RijndaelManaged();

                symmetricKey.Mode = CipherMode.CBC;

                ICryptoTransform decryptor = symmetricKey.CreateDecryptor
                (
                    keyBytes,
                    initVectorBytes
                );

                memoryStream = new MemoryStream(cipherTextBytes);

                CryptoStream cryptoStream = new CryptoStream
                (
                    memoryStream,
                    decryptor,
                    CryptoStreamMode.Read
                );

                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                int decryptedByteCount = cryptoStream.Read
                (
                    plainTextBytes,
                    0,
                    plainTextBytes.Length
                );


                cryptoStream.Close();

                string plainText = Encoding.UTF8.GetString
                (
                    plainTextBytes,
                    0,
                    decryptedByteCount
                );

                return plainText;
            }
            catch (Exception)
            {
                return "";
            }
            finally
            {
                memoryStream.Close();
            }
        }

        public static bool SendEmail(string PrimaryEmail, string bodyOfEmail, string subjectOfEmail, string CCEmail = "")
        {
            try
            {
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(PrimaryEmail);
                if (CCEmail != "")
                {
                    mail.CC.Add(CCEmail);
                }

                mail.Subject = subjectOfEmail;
                mail.Body = bodyOfEmail;
                mail.IsBodyHtml = true;
                mail.Priority = System.Net.Mail.MailPriority.Normal;

                client.Timeout = 2000000;
                client.Send(mail);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}