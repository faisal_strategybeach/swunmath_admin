﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Swunmath
{
    public class CategoryBL
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
        SqlConnection studentDBConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["StudentDBConnection"].ConnectionString);

        public bool SaveCategory(Model.CategoryModel objCategory)
        {

            SqlCommand cmd = new SqlCommand("AddCategory", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", objCategory.Id);
            cmd.Parameters.AddWithValue("@CategoryName", objCategory.CategoryName);
            cmd.Parameters.AddWithValue("@Status", objCategory.Status);
            cmd.Parameters.AddWithValue("@CreatedBy", objCategory.User);
            cn.Open();
            int i = cmd.ExecuteNonQuery();
            cn.Close();

            //Make changes to Student DB as well
            if (studentDBConnection.ConnectionString.Trim() != "")
            {
                cmd = new SqlCommand("AddCategory", studentDBConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", objCategory.Id);
                cmd.Parameters.AddWithValue("@CategoryName", objCategory.CategoryName);
                cmd.Parameters.AddWithValue("@Status", objCategory.Status);
                cmd.Parameters.AddWithValue("@CreatedBy", objCategory.User);
                studentDBConnection.Open();
                i = cmd.ExecuteNonQuery();
                studentDBConnection.Close();
            }

            if (i >= 1)
                return true;
            else
                return false;        

        }
        public List<Model.CategoryModel> GetAllCategory()
        {            

            List<Model.CategoryModel> categorylist = new List<Model.CategoryModel>();

            SqlCommand cmd = new SqlCommand("GetAllCategory", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            cn.Open();
            sd.Fill(dt);
            cn.Close();
            foreach (DataRow dr in dt.Rows)
            {
                categorylist.Add(
                    new Model.CategoryModel
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        CategoryName = Convert.ToString(dr["CategoryName"]),
                        BookCount = Convert.ToInt32(dr["BookCount"]),
                        Status = Convert.ToBoolean(dr["Status"]),
                        CreatedOn = Convert.ToDateTime(dr["CreatedOn"]),
                    });
            }
            return categorylist;
        }
    }
}