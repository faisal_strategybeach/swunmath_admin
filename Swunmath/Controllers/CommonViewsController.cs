﻿using System;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Swunmath.Model;

namespace Swunmath.Controllers
{
    public class CommonViewsController : Controller
    {
        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Error_One()
        {
            return View();
        }

        public ActionResult Error_Two()
        {
            return View();
        }

        public ActionResult LockScreen()
        {
            return View();
        }

        public ActionResult Category()
        {
            CategoryBL cbl = new CategoryBL();
            var cats = cbl.GetAllCategory();
            return View(cats);
        }

        public JsonResult GetCategories()
        {
            CategoryBL cbl = new CategoryBL();
            return Json(cbl.GetAllCategory(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "admin")]
        public ActionResult AddCategory(int id)
        {
            if (id != 0)
            {
                CategoryBL cbl = new CategoryBL();
                return View(cbl.GetAllCategory().Find(m => m.Id == id));
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult AddCategory(Swunmath.Model.CategoryModel objCategory)
        {
            CategoryBL cbl = new CategoryBL();
            objCategory.User = Convert.ToString(Membership.GetUser().ProviderUserKey);
            if (ModelState.IsValid)
            {
                bool val = cbl.SaveCategory(objCategory);
                ModelState.Clear();
                return RedirectToAction("Category", "CommonViews");
            }
            else
            {
                return View();
            }
        }

        [Authorize(Roles = "admin,student")]
        public ActionResult eBook()
        {            
            eBookBL bookobj = new eBookBL();
            var bookcollection = bookobj.GetAlleBook();           
            return View(bookcollection);
        }

        /// <summary>
        /// 1. Uploading PDF Zip file
        /// 2. Extracting zip file
        /// 3. Move the Zip file from old file to Newly created folder name
        /// 4. copy script.min.js from swunmath project to PDFFlip project
        /// 5. copy template.js from swunmath project to PDFFlip project
        /// 5. copy drawingboard.js from swunmath project to PDFFlip project
        /// </summary>
        /// <param name="objeBook"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult AddeBook(Swunmath.Model.eBookModel objeBook)
        {
            try
            {
                Logger.WriteLog("Entered in to AddeBook Method", "Info");
                eBookBL bookbl = new eBookBL();
                objeBook.User = Convert.ToString(Membership.GetUser().ProviderUserKey);

                var folderUploadPath = ConfigurationManager.AppSettings["StudentUploadPath"];
                var thumbnailPath = ConfigurationManager.AppSettings["StudentThumbnailPath"];

                if (objeBook.BookType.ToUpper() == "T")
                {
                    folderUploadPath = ConfigurationManager.AppSettings["TeacherUploadPath"];
                    thumbnailPath = ConfigurationManager.AppSettings["TeacherThumbnailPath"];
                }

                if (objeBook.Id > 0)
                {
                    Model.eBookModel book = bookbl.GetAlleBook().Where(X => X.Id == objeBook.Id).First();
                    string NewFileName = $"grade{GetCategoryName(objeBook.Category)}"; //DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (objeBook.file != null && objeBook.file.ContentLength > 0)
                    {
                        NewFileName = NewFileName + Path.GetExtension(objeBook.file.FileName);
                        string fileName = Path.GetFileName(objeBook.file.FileName);

                        objeBook.file.SaveAs(Path.Combine(folderUploadPath, NewFileName));

                        if (System.IO.File.Exists(Path.Combine(folderUploadPath, NewFileName)))
                        {
                            if (System.IO.Directory.Exists(folderUploadPath + "__MACOSX"))
                            {
                                Directory.Delete(folderUploadPath + "__MACOSX", true);
                            }
                            if (System.IO.Directory.Exists(folderUploadPath + "/" + book.PdfPath))
                            {
                                Directory.Delete(folderUploadPath + "/" + book.PdfPath, true);
                            }
                            ZipFile.ExtractToDirectory(Path.Combine(folderUploadPath, NewFileName), folderUploadPath);
                        }
                        if (System.IO.File.Exists(Path.Combine(folderUploadPath, NewFileName)))
                        {
                            System.IO.File.Delete(Path.Combine(folderUploadPath, NewFileName));
                        }

                        Directory.Move(folderUploadPath + "/" + fileName.Substring(0, fileName.IndexOf(".")), folderUploadPath + "/" + NewFileName.Substring(0, NewFileName.IndexOf(".")));
                        //copy script.min.js from swunmath project to PdfFlip project folder
                        System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["Scriptreplace"]), folderUploadPath + "/" + NewFileName.Substring(0, NewFileName.IndexOf(".")) + ConfigurationManager.AppSettings["Scriptorg"], true);
                        //copy template.js from swunmath project to PdfFlip project folder
                        System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["SourcetemplateFilePath"]), folderUploadPath + "/" + NewFileName.Substring(0, NewFileName.IndexOf(".")) + ConfigurationManager.AppSettings["DestinationtemplateFilePath"], true);

                        //Copy drawing.js file from swunmath scripts folder to PdfFlip DrawingboardZipPath folder
                        CopyDrawingFiles(folderUploadPath + NewFileName.Substring(0, NewFileName.IndexOf(".")) + "\\");
                        
                        //if (System.IO.Directory.Exists(folderUploadPath + "__MACOSX"))
                        //{
                        //    Directory.Delete(folderUploadPath + "__MACOSX", true);
                        //}
                        //if (System.IO.Directory.Exists(folderUploadPath + "/" + book.PdfPath))
                        //{
                        //    Directory.Delete(folderUploadPath + "/" + book.PdfPath, true);
                        //}
                        objeBook.PdfPath = NewFileName.Substring(0, NewFileName.IndexOf("."));



                        //Save Config data in DB
                        string folderName = Path.GetFileNameWithoutExtension(Path.Combine(folderUploadPath, NewFileName));
                        objeBook.ConfigString = System.IO.File.ReadAllText(Path.Combine(folderUploadPath, folderName, "config.js"));
                        objeBook.ConfigString = objeBook.ConfigString.Replace("var config =", "").Trim();

                        //Replace config.js
                        string sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Scripts", "config.js");
                        string destFile = Path.Combine(folderUploadPath, folderName, "config.js");
                        System.IO.File.Copy(sourceFile, destFile, true);
                    }
                    else
                        objeBook.PdfPath = book.PdfPath;

                    if (objeBook.Thumbnail != null && objeBook.Thumbnail.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(objeBook.Thumbnail.FileName);
                        NewFileName = NewFileName.Substring(0, NewFileName.IndexOf(".")) + Path.GetExtension(objeBook.Thumbnail.FileName);
                        var path = Path.Combine(thumbnailPath, NewFileName);
                        objeBook.Thumbnail.SaveAs(path);

                        path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["AdminThumbnailPath"]), NewFileName);
                        objeBook.Thumbnail.SaveAs(path);

                        if (System.IO.File.Exists(Path.Combine(thumbnailPath + book.ThumbnailPath)))
                        {
                            System.IO.File.Delete(Path.Combine(thumbnailPath + book.ThumbnailPath));
                        }
                        if (System.IO.File.Exists(Server.MapPath(ConfigurationManager.AppSettings["AdminThumbnailPath"]) + book.ThumbnailPath))
                        {
                            System.IO.File.Delete(Server.MapPath(ConfigurationManager.AppSettings["AdminThumbnailPath"]) + book.ThumbnailPath);
                        }
                        objeBook.ThumbnailPath = NewFileName;
                    }
                    else
                        objeBook.ThumbnailPath = book.ThumbnailPath;
                    bookbl.SaveeBook(objeBook);
                }
                else if (objeBook.file != null && objeBook.file.ContentLength > 0 && objeBook.Thumbnail != null && objeBook.Thumbnail.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(objeBook.file.FileName);
                    Logger.WriteLog(" fileName : " + fileName, "Info");
                    string NewFileName = $"grade{GetCategoryName(objeBook.Category)}" + Path.GetExtension(objeBook.file.FileName);// DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(objeBook.file.FileName);
                    
                    Logger.WriteLog(" ZipPath : " + folderUploadPath, "Info");
                    objeBook.file.SaveAs(Path.Combine(folderUploadPath, NewFileName));
                    Logger.WriteLog(" Saved in this : " + folderUploadPath, "Info");
                    if (System.IO.File.Exists(Path.Combine(folderUploadPath, NewFileName)))
                    {
                        ZipFile.ExtractToDirectory(Path.Combine(folderUploadPath, NewFileName), folderUploadPath);
                    }


                    Logger.WriteLog(" ExtractToDirectory : " + Path.Combine(folderUploadPath, NewFileName), "Info");
                    if (System.IO.File.Exists(Path.Combine(folderUploadPath, NewFileName)))
                    {
                        System.IO.File.Delete(Path.Combine(folderUploadPath, NewFileName));
                    }
                    Directory.Move(folderUploadPath + "/" + fileName.Substring(0, fileName.IndexOf(".")), folderUploadPath + "/" + NewFileName.Substring(0, NewFileName.IndexOf(".")));
                    Logger.WriteLog("Moved : " + folderUploadPath + "/" + fileName.Substring(0, fileName.IndexOf(".")) + " To : " + folderUploadPath + "/" + NewFileName.Substring(0, NewFileName.IndexOf(".")), "Info");
                    //copy script.min.js from swunmath project to PDFfile project folder
                    System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["Scriptreplace"]), folderUploadPath + "/" + NewFileName.Substring(0, NewFileName.IndexOf(".")) + ConfigurationManager.AppSettings["Scriptorg"], true);
                    //copy template.js from swunmath project to PDFfile project folder
                    System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["SourcetemplateFilePath"]), folderUploadPath + "/" + NewFileName.Substring(0, NewFileName.IndexOf(".")) + ConfigurationManager.AppSettings["DestinationtemplateFilePath"], true);

                    //Copy drawing.js file from swunmath scripts folder to PDFfile DrawingboardZipPath folder
                    CopyDrawingFiles(folderUploadPath + NewFileName.Substring(0, NewFileName.IndexOf(".")) + "\\"); 
                    
                    Logger.WriteLog(folderUploadPath + "/" + "__MACOSX", "Info");
                    if (System.IO.Directory.Exists(folderUploadPath + "__MACOSX"))
                    {
                        Logger.WriteLog(folderUploadPath + "__MACOSX : Exixts", "Info");
                        Directory.Delete(folderUploadPath + "__MACOSX", true);
                        Logger.WriteLog("Deleted", "Info");
                    }

                    objeBook.PdfPath = NewFileName.Substring(0, NewFileName.IndexOf("."));
                    NewFileName = NewFileName.Remove(NewFileName.IndexOf(".")) + Path.GetExtension(objeBook.Thumbnail.FileName);
                    thumbnailPath = Path.Combine(thumbnailPath, NewFileName);
                    Logger.WriteLog("    " + folderUploadPath, "Info");
                    objeBook.Thumbnail.SaveAs(thumbnailPath);
                    Logger.WriteLog("Thumbnail Saved : " + folderUploadPath, "Info");

                    thumbnailPath = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["AdminThumbnailPath"]), NewFileName);
                    Logger.WriteLog("AdminThumbnailPath path : " + thumbnailPath, "Info");
                    objeBook.Thumbnail.SaveAs(thumbnailPath);
                    Logger.WriteLog("AdminThumbnailPath Saved : " + thumbnailPath, "Info");

                    objeBook.ThumbnailPath = NewFileName;
                    Logger.WriteLog("Thumbnail file name : " + objeBook.ThumbnailPath, "Info");



                    //Save Config data in DB
                    string folderName = Path.GetFileNameWithoutExtension(Path.Combine(folderUploadPath, NewFileName));
                    objeBook.ConfigString = System.IO.File.ReadAllText(Path.Combine(folderUploadPath, folderName, "config.js"));
                    objeBook.ConfigString = objeBook.ConfigString.Replace("var config =", "").Trim();

                    //Replace config.js
                    string sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Scripts", "config.js");
                    string destFile = Path.Combine(folderUploadPath, folderName, "config.js");
                    System.IO.File.Copy(sourceFile, destFile, true);


                    bookbl.SaveeBook(objeBook);
                    Logger.WriteLog("Saved in database", "Info");
                }
                Logger.WriteLog("Exited from AddeBook Method","Info");
            }
            catch(Exception ex)
            {
                Logger.WriteLog(ex.ToString(),"Error");
            }
            return RedirectToAction("eBook");
        }

        [Authorize(Roles = "admin")]
        public ActionResult AddeBook(int id)
        {
            eBookBL BL = new eBookBL();
            return id > 0 ? View(BL.GetAlleBook().Where(X => X.Id == id).FirstOrDefault()) : View(new Model.eBookModel());
        }
        /// <summary>
        /// Copy the Drawing Board css and js files to PdfFlip project folder
        /// </summary>
        /// <param name="drawingBoardJsPath"></param>
        private void CopyDrawingFiles(string drawingBoardJsPath)
        {
            var root = drawingBoardJsPath+ConfigurationManager.AppSettings["DrawingboardExtractPath"];
            // Get all subdirectories
            var subDirectories = Directory.GetDirectories(root);
            foreach (var subDirectory in subDirectories)
            {
                if (System.IO.Directory.Exists(subDirectory))
                {
                    //Copy drawing.js file from swunmath scripts folder to PDFfile DrawingboardZipPath folder
                    System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["SourceDrawingJsFilePath"]), subDirectory + "\\js\\drawingboard.js", true);
                    //Copy drawing.css file from swunmath scripts folder to PDFfile DrawingboardZipPath folder
                    System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["SourceDrawingCSSFilePath"]), subDirectory + "\\css\\drawingboard.css", true);

                    System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["SourceDrawingIndexFilePath"]), subDirectory + "\\index.html", true);
                    System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["SourceDrawingJsColorFilePath"]), subDirectory + "\\js\\jscolor.js", true);
                    System.IO.File.Copy(Server.MapPath(ConfigurationManager.AppSettings["SourceDrawingSaveImgFilePath"]), subDirectory + "\\img\\save-icon.png", true);


                }
            } 
        }

        private string GetCategoryName(string catId)
        {
            var categoryBl = new CategoryBL();
            var lstCategory= categoryBl.GetAllCategory().Where(d => d.Status);
            return lstCategory.FirstOrDefault(x => x.Id == Convert.ToInt16(catId)).CategoryName;
        }

    }
}