﻿using Swunmath.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Swunmath.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(string username, string password)
        {
            MembershipUser user = Membership.GetUser(username);
            if (user == null)
            {
                ViewBag.Error = "User does not exist";
                return View("Login");
            }
            else if(user.IsLockedOut)
            {
                ViewBag.Error = "Account has been locked. Please contact the administrator.";
                return View("Login");
            }

            if (Membership.ValidateUser(username, password))
            {
                FormsAuthentication.SetAuthCookie(username, false);
                System.Web.HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddMinutes(20), false, string.Join(",", Roles.GetRolesForUser(username))))));
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                ViewBag.Error = "Invalid username or password";
                return View("Login");
            }
                
        }

        [Route("User/Generatepassword/{EncryyptedID}")]
        public ActionResult Generatepassword(string EncryyptedID)
        {
            Utility U = new Utility();
            MembershipUser user = Membership.GetUser(U.Decrypt(EncryyptedID));
            return View(new UserModel { EmailID = user.Email, IsLocked = user.IsLockedOut, Name = "", Password = "" });
        }

        public ActionResult Changepassword(string UserName, string NewPassword)
        {
            MembershipUser user = Membership.GetUser(UserName);
            user.ChangePassword(user.ResetPassword(), NewPassword);
            return View("ChangeSuccessfull");
        }

        [Authorize(Roles = "admin")]
        public ActionResult ManageUsers()
        {
            string[] users = Roles.GetUsersInRole(ConfigurationManager.AppSettings["AdminRolename"]);
            List<UserModel> usersList = new List<UserModel>();
            MembershipUser LoggedInUser = Membership.GetUser();
            users.ToList().ForEach(X =>
            {
                MembershipUser user = Membership.GetUser(X);
                var profile = System.Web.Profile.ProfileBase.Create(X);
                usersList.Add(new UserModel { Name = Convert.ToString(profile.GetPropertyValue("Name")), CreationDate = user.CreationDate, EmailID = user.Email, IsLocked = user.IsLockedOut, IsLoggedIn = (X == LoggedInUser.UserName) });
            });

            return View(usersList);
        }

        [Authorize(Roles = "admin")]
        public ActionResult AddUser(string UserName)
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult SaveUser(Model.UserModel User)
        {
            MembershipUser user = Membership.GetUser(User.EmailID);
            if (user == null)
            {
                user = Membership.CreateUser(User.EmailID, User.Password, User.EmailID);
                Roles.AddUserToRole(User.EmailID, ConfigurationManager.AppSettings["AdminRolename"]);
                System.Web.Profile.ProfileBase profile = System.Web.Profile.ProfileBase.Create(User.EmailID);
                profile.SetPropertyValue("Name", User.Name);
                profile.Save();
            }
            else
            {
                System.Web.Profile.ProfileBase profile = System.Web.Profile.ProfileBase.Create(User.EmailID);
                profile.SetPropertyValue("Name", User.Name);
                profile.Save();
            }
            return RedirectToAction("ManageUsers");
        }

        public ActionResult PasswordRecovery()
        {
            return View();
        }

        public ActionResult SendRecoveryemail(string UserEmail)
        {
            string Username = Membership.GetUserNameByEmail(UserEmail);
            if (!string.IsNullOrEmpty(Username))
            {
                Utility U = new Utility();
                string Body = "Hello, <br/><br/></br> Please click on the link below to re-generate your password. <br/><br/> <a href='" + string.Format(ConfigurationManager.AppSettings["Passwordrecoverylink"], U.Encrypt(UserEmail) + "'>Click here</a>");

                ViewBag.Message = Utility.SendEmail(UserEmail, Body, ConfigurationManager.AppSettings["RecoverySubject"]) ? "An email with a reset link has been sent to your registered email." : "Some internal error occurred..";
            }
            else
                ViewBag.Message = "Email not registered with us.";

            return View("PasswordRecovery");
        }

        [Authorize(Roles = "admin")]
        public ActionResult LockUnlockUser(string UserName)
        {
            MembershipUser user = Membership.GetUser(UserName);

            if (user.IsLockedOut)
                user.UnlockUser();
            else
            {
                while (!user.IsLockedOut)
                {
                    Membership.ValidateUser(UserName, DateTime.Now.ToString("yyyMMddHHmmss"));
                    user = Membership.GetUser(user.UserName);
                }
            }
            return RedirectToAction("ManageUsers");
        }
    }
}