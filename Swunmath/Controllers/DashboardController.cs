﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Security;

namespace Swunmath.Controllers
{
    [Authorize(Roles = "admin")]
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            CategoryBL Cat = new CategoryBL();
            eBookBL Book = new eBookBL();
            ViewBag.UserCount = Roles.GetUsersInRole(ConfigurationManager.AppSettings["AdminRolename"]).ToList().Count;//Membership.GetAllUsers().Count;
            ViewBag.Categorycount = Cat.GetAllCategory().Count;
            ViewBag.BookCount = Book.GetAlleBook().Count;
            return View();
        }
    }
}
