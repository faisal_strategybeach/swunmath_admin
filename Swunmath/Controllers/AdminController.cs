﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Swunmath.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LogIn()
        {
            return View("~/Views/Admin/Dashboard.cshtml");
        }
        public ActionResult Dashboard()
        {
            return View();
        }
    }
}