﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Swunmath
{

    public class eBookBL
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
        SqlConnection studentDBConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["StudentDBConnection"].ConnectionString);

        public bool SaveeBook(Model.eBookModel objeBook)
        {
            SqlCommand cmd = new SqlCommand("AddeBook", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", objeBook.Id);
            cmd.Parameters.AddWithValue("@eBookId", objeBook.eBookId);
            cmd.Parameters.AddWithValue("@eBookTitle", objeBook.eBookTitle);
            cmd.Parameters.AddWithValue("@Status", objeBook.Status);
            cmd.Parameters.AddWithValue("@Category", objeBook.Category);
            cmd.Parameters.AddWithValue("@PdfPath", objeBook.PdfPath ?? "");
            cmd.Parameters.AddWithValue("@Thumbnailpath", objeBook.ThumbnailPath ?? "");
            cmd.Parameters.AddWithValue("@CreatedBy", objeBook.User);
            cmd.Parameters.AddWithValue("@BookType", objeBook.BookType);
            cmd.Parameters.AddWithValue("@ConfigString", objeBook.ConfigString);
            cn.Open();
            int i = cmd.ExecuteNonQuery();
            cn.Close();


            //Make changes to Student DB as well
            if (studentDBConnection.ConnectionString.Trim() != "")
            {
                cmd = new SqlCommand("AddeBook", studentDBConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", objeBook.Id);
                cmd.Parameters.AddWithValue("@eBookId", objeBook.eBookId);
                cmd.Parameters.AddWithValue("@eBookTitle", objeBook.eBookTitle);
                cmd.Parameters.AddWithValue("@Status", objeBook.Status);
                cmd.Parameters.AddWithValue("@Category", objeBook.Category);
                cmd.Parameters.AddWithValue("@PdfPath", objeBook.PdfPath ?? "");
                cmd.Parameters.AddWithValue("@Thumbnailpath", objeBook.ThumbnailPath ?? "");
                cmd.Parameters.AddWithValue("@CreatedBy", objeBook.User);
                cmd.Parameters.AddWithValue("@BookType", objeBook.BookType);
                cmd.Parameters.AddWithValue("@ConfigString", objeBook.ConfigString);
                studentDBConnection.Open();

                i = cmd.ExecuteNonQuery();
                studentDBConnection.Close();
            }

            return (i > 0);
        }

        public List<Model.eBookModel> GetAlleBook()
        {
            List<Model.eBookModel> ebooklist = new List<Model.eBookModel>();
            SqlCommand cmd = new SqlCommand("GetAlleBook", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            cn.Open();
            sd.Fill(dt);
            cn.Close();
            foreach (DataRow dr in dt.Rows)
            {
                ebooklist.Add(
                    new Model.eBookModel
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        eBookId = Convert.ToString(dr["eBookId"]),
                        eBookTitle = Convert.ToString(dr["eBookTitle"]),
                        Year = Convert.ToString(dr["Year"]),
                        Status = Convert.ToBoolean(dr["Status"]),
                        Category = Convert.ToString(dr["Category"]),
                        PdfPath = Convert.ToString(dr["PdfPath"]),
                        CategoryName = Convert.ToString(dr["CategoryName"]),
                        ThumbnailPath=Convert.ToString(dr["Thumbnailpath"]),
                        BookType = Convert.ToString(dr["BookType"]),
                        ConfigString = Convert.ToString(dr["ConfigString"])
                    });
            }
            return ebooklist;
        }
    }

}