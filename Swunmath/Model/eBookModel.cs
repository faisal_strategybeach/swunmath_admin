﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Swunmath.Model
{
    public class eBookModel
    {
        public int Id { get; set; }
        public string eBookId { get; set; }
        [Required(ErrorMessage = "ebook is required.")]
        public string eBookTitle { get; set; }
        public string Year { get; set; }
        public bool Status { get; set; }
        public string Category { get; set; }
        public string CategoryName { get; set; }
        public HttpPostedFileBase file { get; set; }
        public HttpPostedFileBase Thumbnail { get; set; }
        public string PdfPath { get; set; }
        public string ThumbnailPath { get; set; }
        public string User { get; set; }

        public string BookType { get; set; }

        public string ConfigString { get; set; }
    }
}