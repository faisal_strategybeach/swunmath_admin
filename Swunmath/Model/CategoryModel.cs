﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Swunmath.Model
{
    public class CategoryModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Category is required.")]
        public string CategoryName { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public int BookCount { get; set; }
        public string User { get; set; }
    }
}