﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Swunmath.Model
{
    public class UserModel
    {
        public string Name { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsLocked { get; set; }
        public bool IsLoggedIn { get; set; }
    }
}