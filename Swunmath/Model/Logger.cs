﻿using System;
using System.Configuration;
using System.IO;

namespace Swunmath.Model
{
    public static class Logger
    {
        public static void WriteLog(string errormsg,string errorType)
        {            
            string LoggerfilePath = Convert.ToString(ConfigurationManager.AppSettings["loggerFile"]);
            string LoggerFileName = string.Format("{0}{1}{2}", LoggerfilePath, DateTime.Now.ToString("MMddyyyy"), ".txt");
            try
            {
                if (!System.IO.Directory.Exists(LoggerfilePath))
                {
                    System.IO.Directory.CreateDirectory(LoggerfilePath);
                }
                using (var tw = new StreamWriter(LoggerFileName, true))
                {
                    errormsg = string.Format("{0}:{1}:{2}", errorType, DateTime.Now.ToString("HH:mm:ss"), errormsg);
                    tw.WriteLine(errormsg);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
    }
}